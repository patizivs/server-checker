require "open3"

class ServerChecker
  def self.reachability(client, server)
    stdout, status = Open3.capture2("docker-compose run #{client} ping -c 1 #{server} ")
    stdout
  end

  def self.ssh_connectivity(username, client, server)
    stdout, status = Open3.capture2("docker-compose run #{client} ssh #{username}@#{server} 2>&1")
    status.success?
  end

  def self.http_connectivity(client, server)
    stdout, status = Open3.capture2("docker-compose run #{client} curl -Is http://#{server}| head -n 1")
    stdout
  end

  def self.web_page(client, server)
    stdout, status = Open3.capture2("docker-compose run #{client} curl -sL http://#{server}")
    stdout
  end
end