# Simple server testing application

## Environment
* Docker container "server-a" - server with runnin webservice and ssh server
* Docker container "server-b" - client server with configured ssh keys for accessing server-a
* Host application - prefferable any recent Linux or Mac OS with Docker and Ruby installed.

## Tests
* Reachability test. Verify that Container B is reachable from container A.

* SSH test. Verify that Container B has the appropriate ports and services running, ensure that an SSH
connec on can be established between A and B.

* HTTP test. Verify that Container B has the appropriate ports and services running, ensure that B
accepts HTTP request and can provide a webpage.

## Running tests
* Install required testing libraries for ruby by running
`$ bundle install`

* Start/initialize Docker containers
` $ docker-compose up -d`

* Run tests
`ruby server_checker_test.rb`
* Stop Docker containers
`$ docker-compose down`

In order to view logs for server-a, for example,
`$ docker logs server-a`
