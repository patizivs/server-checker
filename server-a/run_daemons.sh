#!/bin/bash
  
# turn on bash's job control
set -m
  
# start ssh server
/usr/sbin/sshd -De &
  
# Start web server
nginx 