require "minitest/autorun"
require "minitest/reporters"
require_relative './server_checker'
Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

class ServerCheckerTest < Minitest::Test
  def test_reachability
    assert_match "1 packets transmitted, 1 packets received, 0% packet loss", ServerChecker.reachability("server-b", "server-a"), "Should return successful ping message"
  end

  def test_ssh_connectivity_with_valid_credentials
    assert ServerChecker.ssh_connectivity("root", "server-b", "server-a"), "Should return succesfull ssh login message"
  end

  def test_ssh_connectivity_with_invalid_credentials
    refute ServerChecker.ssh_connectivity("bob", "server-b", "server-a"), "Should return succesfull ssh login message"
  end

  def test_http_connectivity
    assert_match "HTTP/1.1 200 OK", ServerChecker.http_connectivity( "server-b", "server-a"), "Should return 200 HTTP STATUS CODE MESSAGE"
  end

  def test_web_page_is_served
    assert_match "Welcome to nginx!", ServerChecker.web_page( "server-b", "server-a"), "Should return web page"
  end
end